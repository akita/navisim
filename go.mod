module gitlab.com/akita/navisim

go 1.13

require (
	//github.com/go-delve/delve v1.6.0 // indirect
	github.com/golang/mock v1.4.3
	github.com/golang/protobuf v1.4.3 // indirect
	github.com/google/go-cmp v0.5.1 // indirect
	github.com/nxadm/tail v1.4.5 // indirect
	github.com/onsi/ginkgo v1.14.2
	github.com/onsi/gomega v1.10.5
	github.com/rs/xid v1.2.1
	github.com/tebeka/atexit v0.3.0
	github.com/vbauerster/mpb/v4 v4.12.2
	gitlab.com/akita/akita v1.10.1
	gitlab.com/akita/dnn v0.3.0
	gitlab.com/akita/mem v1.12.0
	gitlab.com/akita/mgpusim v1.8.1
	gitlab.com/akita/noc v1.4.0
	gitlab.com/akita/util v0.6.3
	golang.org/x/exp v0.0.0-20191030013958-a1ab85dbe136 // indirect
	golang.org/x/net v0.0.0-20210226101413-39120d07d75e // indirect
	golang.org/x/sys v0.0.0-20210223212115-eede4237b368 // indirect
	golang.org/x/text v0.3.5 // indirect
	golang.org/x/xerrors v0.0.0-20200804184101-5ec99f83aff1 // indirect
	google.golang.org/protobuf v1.25.0 // indirect
	gopkg.in/yaml.v2 v2.4.0 // indirect
)
