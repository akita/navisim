// Code generated by "esc -pkg bicg -o esc.go -private kernels.hsaco"; DO NOT EDIT.

package bicg

import (
	"bytes"
	"compress/gzip"
	"encoding/base64"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"os"
	"path"
	"sync"
	"time"
)

type _escLocalFS struct{}

var _escLocal _escLocalFS

type _escStaticFS struct{}

var _escStatic _escStaticFS

type _escDirectory struct {
	fs   http.FileSystem
	name string
}

type _escFile struct {
	compressed string
	size       int64
	modtime    int64
	local      string
	isDir      bool

	once sync.Once
	data []byte
	name string
}

func (_escLocalFS) Open(name string) (http.File, error) {
	f, present := _escData[path.Clean(name)]
	if !present {
		return nil, os.ErrNotExist
	}
	return os.Open(f.local)
}

func (_escStaticFS) prepare(name string) (*_escFile, error) {
	f, present := _escData[path.Clean(name)]
	if !present {
		return nil, os.ErrNotExist
	}
	var err error
	f.once.Do(func() {
		f.name = path.Base(name)
		if f.size == 0 {
			return
		}
		var gr *gzip.Reader
		b64 := base64.NewDecoder(base64.StdEncoding, bytes.NewBufferString(f.compressed))
		gr, err = gzip.NewReader(b64)
		if err != nil {
			return
		}
		f.data, err = ioutil.ReadAll(gr)
	})
	if err != nil {
		return nil, err
	}
	return f, nil
}

func (fs _escStaticFS) Open(name string) (http.File, error) {
	f, err := fs.prepare(name)
	if err != nil {
		return nil, err
	}
	return f.File()
}

func (dir _escDirectory) Open(name string) (http.File, error) {
	return dir.fs.Open(dir.name + name)
}

func (f *_escFile) File() (http.File, error) {
	type httpFile struct {
		*bytes.Reader
		*_escFile
	}
	return &httpFile{
		Reader:   bytes.NewReader(f.data),
		_escFile: f,
	}, nil
}

func (f *_escFile) Close() error {
	return nil
}

func (f *_escFile) Readdir(count int) ([]os.FileInfo, error) {
	if !f.isDir {
		return nil, fmt.Errorf(" escFile.Readdir: '%s' is not directory", f.name)
	}

	fis, ok := _escDirs[f.local]
	if !ok {
		return nil, fmt.Errorf(" escFile.Readdir: '%s' is directory, but we have no info about content of this dir, local=%s", f.name, f.local)
	}
	limit := count
	if count <= 0 || limit > len(fis) {
		limit = len(fis)
	}

	if len(fis) == 0 && count > 0 {
		return nil, io.EOF
	}

	return fis[0:limit], nil
}

func (f *_escFile) Stat() (os.FileInfo, error) {
	return f, nil
}

func (f *_escFile) Name() string {
	return f.name
}

func (f *_escFile) Size() int64 {
	return f.size
}

func (f *_escFile) Mode() os.FileMode {
	return 0
}

func (f *_escFile) ModTime() time.Time {
	return time.Unix(f.modtime, 0)
}

func (f *_escFile) IsDir() bool {
	return f.isDir
}

func (f *_escFile) Sys() interface{} {
	return f
}

// _escFS returns a http.Filesystem for the embedded assets. If useLocal is true,
// the filesystem's contents are instead used.
func _escFS(useLocal bool) http.FileSystem {
	if useLocal {
		return _escLocal
	}
	return _escStatic
}

// _escDir returns a http.Filesystem for the embedded assets on a given prefix dir.
// If useLocal is true, the filesystem's contents are instead used.
func _escDir(useLocal bool, name string) http.FileSystem {
	if useLocal {
		return _escDirectory{fs: _escLocal, name: name}
	}
	return _escDirectory{fs: _escStatic, name: name}
}

// _escFSByte returns the named file from the embedded assets. If useLocal is
// true, the filesystem's contents are instead used.
func _escFSByte(useLocal bool, name string) ([]byte, error) {
	if useLocal {
		f, err := _escLocal.Open(name)
		if err != nil {
			return nil, err
		}
		b, err := ioutil.ReadAll(f)
		_ = f.Close()
		return b, err
	}
	f, err := _escStatic.prepare(name)
	if err != nil {
		return nil, err
	}
	return f.data, nil
}

// _escFSMustByte is the same as _escFSByte, but panics if name is not present.
func _escFSMustByte(useLocal bool, name string) []byte {
	b, err := _escFSByte(useLocal, name)
	if err != nil {
		panic(err)
	}
	return b
}

// _escFSString is the string version of _escFSByte.
func _escFSString(useLocal bool, name string) (string, error) {
	b, err := _escFSByte(useLocal, name)
	return string(b), err
}

// _escFSMustString is the string version of _escFSMustByte.
func _escFSMustString(useLocal bool, name string) string {
	return string(_escFSMustByte(useLocal, name))
}

var _escData = map[string]*_escFile{

	"/kernels.hsaco": {
		name:    "kernels.hsaco",
		local:   "kernels.hsaco",
		size:    13784,
		modtime: 1609513362,
		compressed: `
H4sIAAAAAAAC/+xbzU8bRxR/+2F7MQQSJYeEquo2jUSaxmZtPgqoUTCQkA8gTiAktErp4h0bk/Wuu14j
XAliaKL2kEqVem9Uqff+B3HyRzT0kEMPiRRVVdUD7YFDXe3ujL3rsAGUIEVoftLydt7HvDf7ZvDszM6d
c2PnWYYZBAwOfgfGuhGdMhH8Fndol83rAwEGoQXCEAQA3qXXSB8xXipgPoPt/PDdES+Fg3W7gCu+RvqI
91K3nRUrSJjfQPPgpcSO3aUdad+156bC78DOHZ+Fq89NJQi7B0+eJwv1wF302SEv5V12AvafGB+x1Ulu
3rH7g8PnIQRhV5wWLzE+Mpq87ui2A0Az5ss5JZPSInJOsa75ghyJZNJLMSkmObqpQ2DXZelGIpHwNDIK
WV0bEAk+E2OnRUm8Fb6MDA2phYGwKEbECTmH6jqiKM5lUxlHIxa2ypOl3JyuutQ6XBqDt5UOW2tM1jJF
OVOv6koeacNj4rBHWovJjiUu3rKlCSNjB2Nhi4ASYXI3Vcojj7hjJDGVmJ2aSZ471VHTmsx+5a2gryZK
qNmMNrClaFpWi+hyVlOIeFTV52R1qJhOI6NegaIYk3k5ha4WZXWgplWXp1JE4mAEpeWiavq3Lr+vW/fl
vm6dtuTfvKxm+req279V3f6tGirZrNcJuPR2B/yG0n8hqyhIc9J7JZ0uIPPm3ruY2XsXn+6NiwldQzsc
JdTza3seL6pmdtTIKpMlLZUwMjuKYlhXUNLQ87XfSev3VzYykyiTQ5rpBNhHfI8aejGPReezS0hx5BIW
J43somwifwVv5biJpPIb8iJKGzpxKopdcSyZKOYmR5PXCrXHEeuuS6Y9ElLZuLx0XpXNG7px2wnarjPe
07vd3CS+7dwkTucmr/v7Zuzr1hXo3ITOTejchM5NqGc6N3FJ+redm0Sj0fAr140YAGjngrX1s7/xAs8B
wj/ulF8wdX1ytVuMsrOmZZVPP2k9E3nSeobD60YseNe2anAty7ju40BBQUFBQUFBQUFBsadg8DydgbAz
T2derT8Iv3zxPQA08973ipuu+9baLjjeY+b5QLVarb6N7WcZ2OABNhn4qfI18BsiwCYH/MZRgM07j+5X
+Cr3jRW5wAk/CACXGLZtnbWeGCssL3xSfngKvq1wrLBxEGCzzMIKs8pXWGDWfwQWLHt2oW1d4FloCgaT
UM4/ZWEZ3oV7FY4RNgBgk4fsrwzLwgMhLPFQfbImOHb8Qts6L7AQCIeTQbZlJcS2rjCPmdXg3WA5VA6t
QVl6ysMyY9EgLAdt/SYGmHKoUg6Hk/mz9yvMAZgmPv+q3qsArFYAyp5rt/lP7Cj/Tfs2/4Dzz4CV/7WH
Jz3551eAC67AKl/hrD7AO7nkrD4QZKFJEOw+wLHL0O7qA5ZOkF97+ABYiWMPrrBs6wo8hlX2LlsOWH3C
9geXuDK3BuVQJWDlOsxCoKUlaeU/BMshiwZgOWDlPdDGTxM/f9Ty/mCfXhQUFBQUFBQUFBQUOwX51vzF
YYc24/JRTAOYmvg7fPLWdwzTf/6r6haVsJx8Vz51ZGt/KVXWMuKi8wWTGItHpagknuwsGKlOtGQiQ5PV
TlVdzEXyhr6AUmanY9Arpbv6Ubw/3q30xFIx1Ncr9abjsa6edH8P6pe6P07PxRS5R/kQYCyr3UbGgDg2
NrKT+lVV2U3tr1pHsZ7mz+97+SHM/7OBfwDz73/g5R/G/GcN/GMkW6KX/xHhH/fyr9svt6H6uQOMMz77
vRd99nshqukmgqhS0gqlHEQzWjE6LxfmAf+1+KYBURMtmXZJzmVTEE3puRzSTIgWSjlTnoNoYb5gGs6d
Q2FoSJrtsv92w9BQzL6PzXa7N4dPqHpKVt1bxC9xvBvIsyMzE4nxi8NvZl0s5NrD9jtPUVvjgJfz3uwy
I+OL0Auu8cW4zo2QcdcGAP9WqzqxJ+OL0BMNYQnwcn8JuORkPBIqNtjzDfS9hj18Mv4JPbZl/6+jw33m
BvzP6fhVEMG2HGH4nJ8JNLQfH6eBXlxlQ/eHPGZUfNwTetadexekGB43UP9/KWyRv1F37C6I+CuHmW2e
31Uf+xK2/3wb+/8DAAD//853qWzYNQAA
`,
	},
}

var _escDirs = map[string][]os.FileInfo{}
