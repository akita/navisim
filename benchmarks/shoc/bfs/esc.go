// Code generated by "esc -private -o esc.go -pkg bfs ./kernels.hsaco"; DO NOT EDIT.

package bfs

import (
	"bytes"
	"compress/gzip"
	"encoding/base64"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"os"
	"path"
	"sync"
	"time"
)

type _escLocalFS struct{}

var _escLocal _escLocalFS

type _escStaticFS struct{}

var _escStatic _escStaticFS

type _escDirectory struct {
	fs   http.FileSystem
	name string
}

type _escFile struct {
	compressed string
	size       int64
	modtime    int64
	local      string
	isDir      bool

	once sync.Once
	data []byte
	name string
}

func (_escLocalFS) Open(name string) (http.File, error) {
	f, present := _escData[path.Clean(name)]
	if !present {
		return nil, os.ErrNotExist
	}
	return os.Open(f.local)
}

func (_escStaticFS) prepare(name string) (*_escFile, error) {
	f, present := _escData[path.Clean(name)]
	if !present {
		return nil, os.ErrNotExist
	}
	var err error
	f.once.Do(func() {
		f.name = path.Base(name)
		if f.size == 0 {
			return
		}
		var gr *gzip.Reader
		b64 := base64.NewDecoder(base64.StdEncoding, bytes.NewBufferString(f.compressed))
		gr, err = gzip.NewReader(b64)
		if err != nil {
			return
		}
		f.data, err = ioutil.ReadAll(gr)
	})
	if err != nil {
		return nil, err
	}
	return f, nil
}

func (fs _escStaticFS) Open(name string) (http.File, error) {
	f, err := fs.prepare(name)
	if err != nil {
		return nil, err
	}
	return f.File()
}

func (dir _escDirectory) Open(name string) (http.File, error) {
	return dir.fs.Open(dir.name + name)
}

func (f *_escFile) File() (http.File, error) {
	type httpFile struct {
		*bytes.Reader
		*_escFile
	}
	return &httpFile{
		Reader:   bytes.NewReader(f.data),
		_escFile: f,
	}, nil
}

func (f *_escFile) Close() error {
	return nil
}

func (f *_escFile) Readdir(count int) ([]os.FileInfo, error) {
	if !f.isDir {
		return nil, fmt.Errorf(" escFile.Readdir: '%s' is not directory", f.name)
	}

	fis, ok := _escDirs[f.local]
	if !ok {
		return nil, fmt.Errorf(" escFile.Readdir: '%s' is directory, but we have no info about content of this dir, local=%s", f.name, f.local)
	}
	limit := count
	if count <= 0 || limit > len(fis) {
		limit = len(fis)
	}

	if len(fis) == 0 && count > 0 {
		return nil, io.EOF
	}

	return fis[0:limit], nil
}

func (f *_escFile) Stat() (os.FileInfo, error) {
	return f, nil
}

func (f *_escFile) Name() string {
	return f.name
}

func (f *_escFile) Size() int64 {
	return f.size
}

func (f *_escFile) Mode() os.FileMode {
	return 0
}

func (f *_escFile) ModTime() time.Time {
	return time.Unix(f.modtime, 0)
}

func (f *_escFile) IsDir() bool {
	return f.isDir
}

func (f *_escFile) Sys() interface{} {
	return f
}

// _escFS returns a http.Filesystem for the embedded assets. If useLocal is true,
// the filesystem's contents are instead used.
func _escFS(useLocal bool) http.FileSystem {
	if useLocal {
		return _escLocal
	}
	return _escStatic
}

// _escDir returns a http.Filesystem for the embedded assets on a given prefix dir.
// If useLocal is true, the filesystem's contents are instead used.
func _escDir(useLocal bool, name string) http.FileSystem {
	if useLocal {
		return _escDirectory{fs: _escLocal, name: name}
	}
	return _escDirectory{fs: _escStatic, name: name}
}

// _escFSByte returns the named file from the embedded assets. If useLocal is
// true, the filesystem's contents are instead used.
func _escFSByte(useLocal bool, name string) ([]byte, error) {
	if useLocal {
		f, err := _escLocal.Open(name)
		if err != nil {
			return nil, err
		}
		b, err := ioutil.ReadAll(f)
		_ = f.Close()
		return b, err
	}
	f, err := _escStatic.prepare(name)
	if err != nil {
		return nil, err
	}
	return f.data, nil
}

// _escFSMustByte is the same as _escFSByte, but panics if name is not present.
func _escFSMustByte(useLocal bool, name string) []byte {
	b, err := _escFSByte(useLocal, name)
	if err != nil {
		panic(err)
	}
	return b
}

// _escFSString is the string version of _escFSByte.
func _escFSString(useLocal bool, name string) (string, error) {
	b, err := _escFSByte(useLocal, name)
	return string(b), err
}

// _escFSMustString is the string version of _escFSMustByte.
func _escFSMustString(useLocal bool, name string) string {
	return string(_escFSMustByte(useLocal, name))
}

var _escData = map[string]*_escFile{

	"/kernels.hsaco": {
		name:    "kernels.hsaco",
		local:   "./kernels.hsaco",
		size:    9648,
		modtime: 1617820504,
		compressed: `
H4sIAAAAAAAC/+xa22/b1hn/eHhEMZQjuy2wpVmycEUBp9uk0IrtuEaB+tY4c2THqdtk6VB4DHkkU6Eo
gaI8a4AYy+garQiGDfsDjGEvexiw/AVW975Xz8PQhw5F97S9eXvIQzkcXiSStXJBG6DO9APEn853O993
Dg/By7n7Vv4yYpgZ8MHCp8DQPyNeO1A8eMXji65sCniYgSEQgAMAHLKL88dMlHlfzvh+/fBaOspBPtQv
EWrHeQZHOexHcwXRl8e4ClEO/NBT+gX1vf25peIn8AvnR3H9c0vl4OmBg/FE0Es8xH9MRRmH/Hi//9nl
Bdc8mJvvuOeDJ8eQBCGUJ5XNLi8srr7r2Z4GgJQvl8tqUTEyclmlv42anMkUC1tj0pjk2f4gBW4sapvJ
ZIQbxKxpFWNaDPATceyHoiS+L1wlpkH02rQgihlxRS6Tno0oinOX19bvuBbrP5PNqkBla43y7YoeMh2N
Wc3cUUddy7xsFOtysRfyWpUY83lxPqLt5ubmlBPfd7WzZtFNiuKIxHSySfSaEDTfaVRJxGa0rhnW90e7
Bmvaz6MBprqqWV0rGtNHqm7Iep1c1Qw1UC/qlduyPlcvFIjZC6Cq5lpVVsj1uqxPd616ekUJNB4WSEGu
61b/6ohaJLOmKTee+wJn61vPa40319fe61+bZlj96xrvX9d4/7rmGq7oK6Q8f+XdlavHL22jXr5BTEtT
yCOuCPVvZOpK3TSP2WgXdLn4iDV7jJbs15TcFU1VieF1fq1QqBHrx8++i1vPvov3nk0XKxWDPOEcDnr+
yj0v13VLWzQ1da1hKLNm8YmymK+oZNWsVLt3YPQuUTaLa6RYJoblJTgmBZeZRbNSr/q6y9oWUT0DyVev
mtqmbJH+BtHofo1BZTflTVIwK0Gvongx52tW6uW1xdW3a93xuDje09yIaMYmfM2yvHVZl62bFfOOl7Ub
NDcxKWSzWeGR9//0vvw0y3WfJ/JsTx78TveeCBBtv/wv+5/IFzDxgLHbZhjguQTTnX/BOweYx9gzf/rp
r+mzHo6ejxuh/6/AcPTZFOOE4zjON7P+33VY4A9PATycR3AoATxEDBxigId3P77fgV3hNyCkWtjB92gF
KSH1Sx7zv00NIXs79YsWcxPZDkIjXzh3rzHD2GagfHBiBDAL2n4CI9hFaYkZ5peENEyzUNpHSQR4mJ9G
SbyEoLSPEwioLfo2AE5y+Rbml1Lch80UJ6ygExixKQ4FMtpGDJ5GDJcH0PZZls4dzjPALzEtptXCeAnx
7eY2xq8iYPYR+wdmm23tISgfQIJjIYGapTd29t6BdgfRupP8IQA8ZFn+cIQygw/PU6axGQC2hVpMkstz
sNOxObvdAliCxAfNEsO0bcb+aA7anV1AEgbnrzuAgI4XLg0fnOERnBWEVdiWPsHQxNU373dY/oNm6Y0P
9/7ttDuBPbX9Lo/gnGs73rVNnDiTZ8/uNO2Sfe8fTrvDsoK9zd7bS8JO5zPn9x07bbdBEJYSZ+83S8lk
207aHx3QuKmhpW2UtjkafwgBVxo+GBpCcDKddnPhoMnR+LtDaSlsc2YIwdmeDU9t/jxC8/3V3mdOu4MQ
shPoRbuFvmVjNGLDdvUTjmkCZT7VhL87Ox2AVgdg9//wN8AAAwwwwAADDDDAAAMcFwTfmh+c9Nj/PAyn
fE4Ebf87fPDU/4LP//nCqVD+9GT0uzKkj+4vrxl3iDkt5vML4lguK2Ul8fyFmqlcIFsWMQ1Zv6Drm+VM
1ayUiGJd0HVVVGUld7ugEFmeUsfl16XcxCVVmZIn1YmpQm5yioxdyilEkshrAIouG0Vx0/tm+yTxPYen
6OFR71HoaJ5/MSpP+vKZmPykL38Qk7/ky/8Sk7/sy6+8FJWfC2ZxJCp/y33Tl+ztO/Ax0ec9IWSNikUg
qzaMWqMM2aJRz27ItQ3wj1RumZC1yJbltuSypkBWqZTLxLAgW2uULfk2ZGsbNcv0/nkMc3PSes49XnSP
k+7xknt8Pf6a8VW9osj6l14+ri/cWpld/tH81/O+KxnaHtFvn0R4X0V8PlMht2DdBCyF1g0T2g8SrKdh
APiv41QC/2DdBCzG0uLhy+dBIpx/OsqnYv44xuf8PR0otq4DfuHI87qH0fBeGui//6ZfgIzvG5x2/fbF
JGL1B91M+iGlWDdV37/Tp/uA3wyPXQjS9zz+G/Sug/wR87cYzj0E3t8fdesx43e9j7/s+688xv9/AQAA
//+FoyMEsCUAAA==
`,
	},
}

var _escDirs = map[string][]os.FileInfo{}
